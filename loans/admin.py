from django.contrib import admin
from .models import Loan_application, Loan
# Register your models here.

admin.site.register(Loan_application)
admin.site.register(Loan)
